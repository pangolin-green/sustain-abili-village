#!/bin/bash
# Fast build for development.

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
SLURP="python3.6 $SCRIPTPATH/../src/JSUtils/Slurp/slurp/build"
SRC=$1
DIST=$2
CONFIG=${3:-config/myapp.config.json}

$SLURP/fileMonitor.py --ignore=node_modules,env,Slurp,documentation,scripts --destinationPath=$DIST $SRC \
| $SLURP/spit.py --fileExtensions=png,jpg,jpeg,svg \
| $SLURP/fileLinker.py --fileExtensions=js,html,css --maxDepth=15 \
| $SLURP/envVars.py --injectversion $CONFIG \
| $SLURP/spit.py
