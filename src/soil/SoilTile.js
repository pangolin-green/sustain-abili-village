
/**
 * @class
 */
class SoilTile {

    constructor() {

        this.clay = 1;
        this.sand = 1;
        this.loam = 1;
        this.rock = 1;

        this.phosphorus = 1;
        this.nitrogen = 1;
        this.potassium = 1;
    }
}