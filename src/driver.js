import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');
import('Sustainabilivillage.js');

/**
 * @class
 */
class HomepageViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();

        Sustainabilivillage.init(ID.CANVAS);
    }
}

Page.addLoadEvent(() => {
    HomepageViewDriver.init();
});

const ID = {
    CANVAS: 'canvas',
    TEMPLATE: {
        
    }
};

const CLASS = {

};
