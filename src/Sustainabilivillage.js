import('/src/jsgame/CollisionGroup.js');
import('/src/jsgame/Game.js');

/**
 * @class
 */
class Sustainabilivillage extends Game {



    static init() {
        super.init(...arguments);

        // Start preloading.
        // JSAudio.load(this._BG_MUSIC_URI);

        Mouse.onRelease(() => {
            this._tryStart();
        });


        // ConfigurableController
        //     .setInput(
        //         this.INPUT.PAUSE,
        //         new KeyDownBooleanInput(KEYS.ESCAPE_KEY),
        //     )
        //     .setInput(
        //         this.INPUT.RESEARCH,
        //         new KeyDownBooleanInput(KEYS.R_KEY),
        //     );

        // ConfigurableController.onInputReleased(this.INPUT.PAUSE, () => {
        //     this.togglePause(this._menu);
        // });

        // ConfigurableController.onInputReleased(this.INPUT.RESEARCH, () => {
        //     this.togglePause(this._techTree);
        // });
        
    }

    static _tryStart() {
        if (this._started) return;

        this._started = true;

        // this.currentRoom.setMusic(this._BG_MUSIC_URI);

        this.currentRoom.load().then(() => {

        });
    }


    static draw() {
        const graphics = this.graphics;

        graphics.background(this._BG_COLOR);

        if (!this._started) {
            graphics.fill(255);
            graphics.noStroke();

            graphics.textAlign(Shapes.ALIGN.CENTER, Shapes.ALIGN.CENTER);

            graphics.textSize(24);
            graphics.text('Sustain-abili-village', this._VIEW_WIDTH / 2, this._VIEW_HEIGHT / 2);
            
            graphics.textSize(12);
            graphics.text('An open-source game for charity', this._VIEW_WIDTH / 2, this._VIEW_HEIGHT / 2 + 32);
            graphics.text('By The Pangolin Green Foundation', this._VIEW_WIDTH / 2, this._VIEW_HEIGHT / 2 + 48);
        }

        super.draw();

        if (this._paused) {
            this._activePauseMenu.draw();
        }

        this.frame++;
    }
}


Sustainabilivillage.frame = 0;

Sustainabilivillage._BG_COLOR = '#8fbc8f';
Sustainabilivillage._BG_MUSIC_URI = 'edm-detection-mode.mp3';

Sustainabilivillage._started = false;

Sustainabilivillage.INPUT = {
    PAUSE: 'pause',
};